class blackHole
{
 PVector position;
 
 blackHole()
 {
  position = new PVector(0,0);
 }
 
 void render()
 {
   if(frameCount == 150)
  {
    position.x = randomGaussian() * 200;
    position.y = randomGaussian() * 200;
    frameCount = 0;
  }
  
   
   circle(position.x, position.y, 50); //SCALE 50
   noStroke(); 
   fill(255, 255, 255); //WHITE
 }
}
