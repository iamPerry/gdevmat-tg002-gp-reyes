void setup()
{
 size(1500, 750, P3D);
 
 
 camera(0, 0, -(height / 2) / tan(PI * 30/ 180), // camera postion
 0, 0, 0, //eye position
 0, -1,0); // up vector
 

}

blackHole bh = new blackHole();
matter m = new matter();

void draw()
{
  println(frameCount);
  background(0);
  m.render();
  bh.render();
  m.move();
}
