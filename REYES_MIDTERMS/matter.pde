class matter
{
 PVector[] position = new PVector[100];
 float[] r = new float[100];
 float[] g = new float[100];
 float[] b = new float[100];
 float[] scale = new float[100];
 
 matter()
 {
   for(int i=0; i<100; i++)
   {
     position[i] = new PVector();
     position[i].x = randomGaussian() * 300;
     position[i].y = randomGaussian() * 300;
     
     r[i] = random(155, 256);
     g[i] = random(155, 256);
     b[i] = random(155, 256);
     scale[i] = random(30);
   }
 }
 
 void render()
 {
   
   if(frameCount == 150)
   {
     for(int i=0; i<100; i++)
     {
       position[i] = new PVector();
       position[i].x = randomGaussian() * 300;
       position[i].y = randomGaussian() * 300;
       
       r[i] = random(155, 256);
       g[i] = random(155, 256);
       b[i] = random(155, 256);
       scale[i] = random(30);
     }
   }
   
   noStroke();
  
  for(int i=0; i<100; i++)
  {
   circle(position[i].x, position[i].y, scale[i]); //SCALE 30
   fill(r[i], g[i], b[i]); //random colors
  }
 }
 
 void move()
 {
   for(int i=0; i<100; i++)
   {
     //get magnitude
    float mx = position[i].x - bh.position.x;
    float my = position[i].y - bh.position.y;
    float mag = sqrt((mx*mx) + (my*my));
  
    //normalization
    float nx = mx / mag;
    float ny = my / mag;
  
    //let velocity be (2.5)
    float velocity = 10;
  
    //new coordinates
    float newX = nx * velocity;
    float newY = ny * velocity;
  
    position[i].sub(new PVector(newX, newY));
   }
 }
}
