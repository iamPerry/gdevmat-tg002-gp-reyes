void setup()
{
    size(1920, 1080, P3D);
  //size(1080, 720, P3D); //for laptop users
  //P3D is for 3d environment

  camera(0, 0, -(height / 2) / tan(PI * 30/ 180), // camera postion
  0, 0, 0, //eye position
  0, -1,0); // up vector
}

walker Walker = new walker();

void draw()
{
  Walker.render();
  
  //float randomValue = floor(random(4)); //floor() gets the floating value of float
  //println(randomValue);
  Walker.randomWalk();
  
}
