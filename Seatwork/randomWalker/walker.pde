class walker
{
  float xPosition;
  float yPosition;
  
  void render()
  {
    circle(xPosition, yPosition, 30);
    fill(random(255), random(255), random(255));
    noStroke();
  }
  
  void randomWalk()
  {
    int decision = floor(random(8));
    
    if(decision == 0) yPosition+=5; //go up
    if(decision == 1) yPosition-=5; //go down
    if(decision == 2) xPosition+=5; //go right
    if(decision == 3) xPosition-=5; //go left
    if(decision == 4)
    {
      //go upper right
     yPosition+=5;
     xPosition+=5;
    }
    if(decision == 5)
    {
      //go lower right
     yPosition-=5;
     xPosition+=5;
    }
    if(decision == 6)
    {
      //go upper left
     yPosition+=5;
     xPosition-=5;
    }
    if(decision == 7)
    {
      //go lower right
     yPosition-=5;
     xPosition-=5;
    }
  }
}
