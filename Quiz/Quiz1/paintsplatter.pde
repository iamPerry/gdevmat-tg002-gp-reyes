void setup()
{
  size(1080, 720, P3D);
  
  camera(0, 0, -(height / 2) / tan(PI * 30/ 180), // camera postion
  0, 0, 0, //eye position
  0, -1,0); // up vector
}

spawn Spawn = new spawn();

void draw()
{
  if(frameCount == 1000)
  {
   frameCount = 0;  
   background(200);
  }
  
  Spawn.render();
  Spawn.randomSpawn();
}
