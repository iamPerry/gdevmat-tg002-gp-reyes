class spawn
{
 float xPos, yPos;
 
 void render()
 {
  fill(random(200, 256), random(200, 256), random(200, 256));
  noStroke();
  circle(xPos, yPos, randomGaussian() * 30);
 }
 
 void randomSpawn()
 {
  xPos = randomGaussian()* 1080;
  yPos = random(-720, 720);
 }
}
